#pragma once

#include <memory>

class AccepterPoolImpl;

class AccepterPool
{
public:
    AccepterPool();
    ~AccepterPool();

private:
    std::unique_ptr<AccepterPoolImpl> mImpl;
};