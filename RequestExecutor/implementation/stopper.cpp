#include "stopper.h"

#include "state_keeper.h"

Stopper::operator bool() const
{
    return ( StateKeeper::Instance().GetState() == ssStopped );
}