#include "acceptor_pool.h"

#include "executor_pool.h"
#include "request.h"
#include "request_task.hpp"
#include "state_keeper.h"
#include "stopper.h"

#include <Windows.h>

#include <algorithm>
#include <assert.h>
#include <thread>
#include <vector>

class AccepterPoolImpl
{
public:
    AccepterPoolImpl ()
    {
        const uint32_t thread_num = StateKeeper::Instance().GetAcceptorThreadsCnt ();
        for ( uint32_t i = 0; i != thread_num && !mStopper; ++i )
            mAcceptors.emplace_back ( std::thread ( &AccepterPoolImpl::Executor, this ) );
    }

    ~AccepterPoolImpl ()
    {
        std::for_each ( mAcceptors.begin (), mAcceptors.end (), []( std::thread& thr ) { thr.join (); } );
    }

    void Executor ()
    {
        while ( !mStopper )
        {
            std::shared_ptr<Request> req ( GetRequest ( mStopper ), DeleteRequest );
            if ( !req || mStopper )
                break;
            ExecutorPool::Instance ().AddTask ( CreateRequestTask ( req, mStopper, ProcessRequest ) );
        }
    }

private:
    Stopper                  mStopper;
    std::vector<std::thread> mAcceptors;
};

AccepterPool::AccepterPool () : mImpl ( new AccepterPoolImpl () )
{
}

AccepterPool::~AccepterPool()
{
}