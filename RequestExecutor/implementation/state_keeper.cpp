#include "state_keeper.h"

// as soon as we use only windows we can use 32 byte type without sync,
// cause read/write operations are atomic. I believe, we can wait for cache sync interval to stop programm

StateKeeper& StateKeeper::Instance ()
{
    // as soon as we use c++11
    static StateKeeper instance;
    return instance;
}

StateKeeper::StateKeeper () : 
    mState ( ssWorking ), mAcceptorThreadsCnt ( 0 ), mExecutorThreadsCnt ( 0 )
{
}

void StateKeeper::SetState ( SystemState st )
{
    mState = st;
}

SystemState StateKeeper::GetState ()
{
    return mState;
}

void StateKeeper::SetAcceptorThreadsCnt ( const uint32_t cnt )
{
    mAcceptorThreadsCnt = cnt;
}

uint32_t StateKeeper::GetAcceptorThreadsCnt ()
{
    return mAcceptorThreadsCnt;
}

void StateKeeper::SetExecutorThreadsCnt ( const uint32_t cnt )
{
    mExecutorThreadsCnt = cnt;
}

uint32_t StateKeeper::GetExecutorThreadsCnt ()
{
    return mExecutorThreadsCnt;
}