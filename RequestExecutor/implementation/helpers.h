#pragma once

#include <string>

template <typename T, int size>
inline int ArraySize ( T ( & )[size] )
{
    return size;
}

int GenerateRandom ( unsigned int upper_bound );

unsigned long GetLastWinApiError();