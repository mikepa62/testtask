#include "acceptor_pool.h"
#include "helpers.h"
#include "state_keeper.h"

#include <Windows.h>

#include <assert.h>
#include <iostream>
#include <string>
#include <thread>

void WorkThread ()
{
    AccepterPool acc_pool;
}

static void ShowUsage ( const std::string& name )
{
    std::cerr << "Usage: " << name << " <option(s)>"
              << "Options:\n"
              << "\t-h\t\tShow this help message\n"
              << "\t-at [COUNT]\tSpecify the count of threads which accepting requests\n"
              << "\t-et [COUNT]\tSpecify the count of threads which executing requests\n"
              << "\n";
}

char* GetCmdOption ( char** begin, char** end, const std::string& option )
{
    char** itr = std::find ( begin, end, option );
    if ( itr != end && ++itr != end )
    {
        return *itr;
    }
    return 0;
}

bool GetUIntCmdOption ( char** begin, char** end, const std::string& option, uint32_t& res )
{
    char* cmd_opt_str = GetCmdOption ( begin, end, option );
    if ( !cmd_opt_str )
        return false;
    long cmd_val = atol ( cmd_opt_str );
    if ( cmd_val <= 0 )
        return false;
    res = static_cast<uint32_t> ( cmd_val );
    return true;
}

bool CmdOptionExists ( char** begin, char** end, const std::string& option )
{
    return std::find ( begin, end, option ) != end;
}

bool ParseParams ( int argc, char* argv[] )
{
    if ( CmdOptionExists ( argv, argv + argc, "-h" ) )
    {
        ShowUsage ( argv[0] );
        return false;
    }
    const unsigned int DEFAULT_ACC_EXEC_THREAD_COUNT = 4;
    // why not?
    unsigned int       hardware_conc_threads = std::thread::hardware_concurrency ();
    if ( hardware_conc_threads == 0 )
        hardware_conc_threads = DEFAULT_ACC_EXEC_THREAD_COUNT;

    uint32_t acceptor_threads = hardware_conc_threads / 2;
    uint32_t executor_threads = hardware_conc_threads - acceptor_threads;

    if ( !CmdOptionExists ( argv, argv + argc, "-at" ) || !GetUIntCmdOption ( argv, argv + argc, "-at", acceptor_threads ) )
    {
        std::cout << "Using default number (" << acceptor_threads << ") of accepting threads. \n";
    }

    if ( !CmdOptionExists ( argv, argv + argc, "-et" ) || !GetUIntCmdOption ( argv, argv + argc, "-et", executor_threads ) )
    {
        std::cout << "Using default number (" << executor_threads << ") of executing threads. \n";
    }

    StateKeeper::Instance ().SetAcceptorThreadsCnt ( acceptor_threads );
    StateKeeper::Instance ().SetExecutorThreadsCnt ( executor_threads );

    return true;
}

int main ( int argc, char* argv[] )
{
    if ( !ParseParams ( argc, argv ) )
        return 0;

    int         res = 0;
    std::thread thread ( WorkThread );
    // 30 seconds
    const DWORD TOTAL_WORKING_TIME = 30 * 1000;

    switch ( ::WaitForSingleObjectEx ( thread.native_handle (), TOTAL_WORKING_TIME, FALSE ) )
    {
        case WAIT_OBJECT_0:
            // succeed finish
            break;
        case WAIT_ABANDONED:
        case WAIT_TIMEOUT:
            // we just want to stop
            break;
        case WAIT_FAILED:
            res = static_cast<int> ( GetLastWinApiError () );
            break;
        default:
            assert ( false );
            res = 1;
            break;
    }
    StateKeeper::Instance ().SetState ( ssStopped );
    thread.join ();
    return res;
}