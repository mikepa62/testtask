#pragma once

#include <string>

class Stopper;

// sample Request class

class Request
{
public:
    ~Request()
    {}

    virtual std::string GetText() const
    {
        return "Test Request";
    }
};

Request* GetRequest( Stopper stopSignal );

void ProcessRequest ( Request* request, Stopper stopSignal );

void DeleteRequest ( Request* request );
