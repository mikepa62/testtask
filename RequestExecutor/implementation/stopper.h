#pragma once

class Stopper
{
public:
    // returns true if the program terminating
    operator bool() const;
};