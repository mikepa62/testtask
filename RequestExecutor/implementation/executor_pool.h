#pragma once

#include <memory>

class ExecutorPoolImpl;
class IRequestTask;

class ExecutorPool
{
public:
    static ExecutorPool& Instance ();
    void AddTask( std::unique_ptr<IRequestTask>&& rt );

private:
    ExecutorPool();
    std::unique_ptr<ExecutorPoolImpl> mImpl;
};