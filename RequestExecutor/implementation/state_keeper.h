#pragma once

#include <cstdint>

enum SystemState : std::int32_t
{
    ssWorking = 0,
    ssStopped
};

class StateKeeper
{
public:
    static StateKeeper& Instance ();
    StateKeeper ();

    void        SetState ( SystemState st );
    SystemState GetState ();

    void        SetAcceptorThreadsCnt ( const uint32_t cnt );
    uint32_t    GetAcceptorThreadsCnt ();

    void        SetExecutorThreadsCnt ( const uint32_t cnt );
    uint32_t    GetExecutorThreadsCnt ();

private:
    SystemState mState;
    uint32_t    mAcceptorThreadsCnt;
    uint32_t    mExecutorThreadsCnt;
};