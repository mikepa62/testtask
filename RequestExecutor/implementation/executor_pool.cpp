#include "executor_pool.h"

#include "fifo_queue.hpp"
#include "request.h"
#include "request_task.hpp"
#include "state_keeper.h"
#include "stopper.h"

#include <algorithm>
#include <vector>
#include <thread>

static const size_t MAX_REQUEST_BUFFER_SIZE = 10000;

class ExecutorPoolImpl
{
public:
    ExecutorPoolImpl () : mQueue ( MAX_REQUEST_BUFFER_SIZE )
    {

        const uint32_t thread_num = StateKeeper::Instance().GetAcceptorThreadsCnt ();
        for ( uint32_t i = 0; i != thread_num && !mStopper; ++i )
            mExecutors.emplace_back ( std::thread ( &ExecutorPoolImpl::Executor, this ) );
    }

    ~ExecutorPoolImpl ()
    {
        mQueue.Close ();
        std::for_each ( mExecutors.begin (), mExecutors.end (), []( std::thread& thr ) { thr.join (); } );
    }

    void AddTask ( std::unique_ptr<IRequestTask>&& rt )
    {
        mQueue.Enqueue ( std::move ( rt ) );
    }

    void Executor ()
    {
        while ( !mStopper )
        {
            std::unique_ptr<IRequestTask> rt;
            mQueue.Dequeue ( std::move ( rt ) );
            if ( !rt || mStopper )
                break;
            rt->Process ();
        }
    }

private:
    FIFOQueue<std::unique_ptr<IRequestTask> > mQueue;
    Stopper                                   mStopper;
    std::vector<std::thread>                  mExecutors;
};

ExecutorPool& ExecutorPool::Instance ()
{
    // as soon as we use c++11
    static ExecutorPool instance;
    return instance;
}

ExecutorPool::ExecutorPool () : mImpl ( new ExecutorPoolImpl () )
{
}

void ExecutorPool::AddTask ( std::unique_ptr<IRequestTask>&& rt )
{
    mImpl->AddTask ( std::move ( rt ) );
}