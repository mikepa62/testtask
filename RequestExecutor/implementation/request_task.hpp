#pragma once

#include <memory>

class IRequestTask
{
public:
    virtual void Process () = 0;
    virtual ~IRequestTask () {};
};

template <typename R, typename St, typename Pf>
class RequestTask : public IRequestTask
{
public:
    RequestTask ( std::shared_ptr<R>& req, St /*stopper*/, Pf pr_func )
        : mReq ( req ), mProcessRequestFunc ( pr_func )
    {
    }

    virtual ~RequestTask () 
    {}

    void Process ()
    {
        mProcessRequestFunc ( mReq.get(), mStopper );
    }

private:
    std::shared_ptr<R> mReq;
    St                 mStopper;
    Pf                 mProcessRequestFunc;
};

template <typename R, typename St, typename Pf>
std::unique_ptr<IRequestTask> CreateRequestTask ( std::shared_ptr<R>& req, St stopper, Pf pr_func)
{
    return std::unique_ptr<IRequestTask> ( new RequestTask<R, St, Pf> ( req, stopper, pr_func ) );
}