#pragma once

#include "helpers.h"

#include <Windows.h>

#include <assert.h>
#include <cstdint>
#include <queue>

class GuardCS
{
public:
    GuardCS ( CRITICAL_SECTION& cs ) : mCs ( cs )
    {
        ::EnterCriticalSection ( &mCs );
    }
    ~GuardCS ()
    {
        ::LeaveCriticalSection ( &mCs );
    }
    GuardCS ( const GuardCS& ) = delete;
    GuardCS& operator= ( const GuardCS& ) = delete;

private:
    CRITICAL_SECTION& mCs;
};

// bounded blocking FIFO queue
template <typename T>
class FIFOQueue
{
public:
    FIFOQueue ( const long max_size )
    {
        mEmptySem = ::CreateSemaphore ( 0, 0, MAXLONG, 0 );
        mOverSem = ::CreateSemaphore ( 0, max_size, MAXLONG, 0 );
        mStopEv = ::CreateEvent ( 0, 1, 0, 0 );
        ::InitializeCriticalSection ( &mCriticalSection );
        mOpened = true;
    }

    ~FIFOQueue ()
    {
        if ( mEmptySem )
        {
            if ( !::CloseHandle ( mEmptySem ) )
            {
                GetLastWinApiError ();
            }
        }
        if ( mOverSem )
        {
            if ( !::CloseHandle ( mOverSem ) )
            {
                GetLastWinApiError ();
            }
        }
        if ( mStopEv )
        {
            if ( !::CloseHandle ( mStopEv ) )
            {
                GetLastWinApiError ();
            }
        }
        ::DeleteCriticalSection ( &mCriticalSection );
    }

    void Enqueue ( T&& elem )
    {
        if ( !mOpened )
            return;
        HANDLE hdls[] = {mOverSem, mStopEv};
        switch ( ::WaitForMultipleObjects ( ArraySize ( hdls ), hdls, FALSE, INFINITE ) )
        {
            case WAIT_OBJECT_0:
                break;
            case WAIT_OBJECT_0 + 1:
            case WAIT_ABANDONED:
            case WAIT_TIMEOUT:
                return;
            case WAIT_FAILED:
                GetLastWinApiError ();
                return;
            default:
                assert ( false );
                return;
        }
        {
            GuardCS gcs ( mCriticalSection );
            if ( !mOpened )
                return;
            mQueue.push ( std::move ( elem ) );
        }
        if ( !::ReleaseSemaphore ( mEmptySem, 1, 0 ) )
            GetLastWinApiError ();
        return;
    }

    void Dequeue ( T&& elem )
    {
        if ( !mOpened )
            return;
        HANDLE hdls[] = {mEmptySem, mStopEv};
        switch ( ::WaitForMultipleObjects ( ArraySize ( hdls ), hdls, FALSE, INFINITE ) )
        {
            case WAIT_OBJECT_0:
                break;
            case WAIT_OBJECT_0 + 1:
                return;
            case WAIT_ABANDONED:
            case WAIT_TIMEOUT:
                // TODO: more logging
                return;
            case WAIT_FAILED:
                GetLastWinApiError ();
                return;
            default:
                assert ( false );
                return;
        }
        {
            GuardCS gcs ( mCriticalSection );
            if ( !mOpened )
                return;
            elem = std::move ( mQueue.front () );
            mQueue.pop ();
        }
        if ( !::ReleaseSemaphore ( mOverSem, 1, 0 ) )
            GetLastWinApiError ();
        return;
    }

    void Close ()
    {
        GuardCS gcs ( mCriticalSection );
        if ( !mOpened )
            return;
        mOpened = false;
        if ( !::SetEvent ( mStopEv ) )
            GetLastWinApiError ();
    }

private:
    // another place of using atomic property of alligned 32 bytes type
    uint32_t         mOpened;
    std::queue<T>    mQueue;
    HANDLE           mEmptySem;
    HANDLE           mOverSem;
    HANDLE           mStopEv;
    CRITICAL_SECTION mCriticalSection;
};