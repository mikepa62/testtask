#include "request.h"

#include "helpers.h"
#include "stopper.h"

#include <Windows.h>

Request* GetRequest ( Stopper stopSignal )
{
    while ( !stopSignal )
    {
        ::Sleep ( GenerateRandom ( 5 ) );
        if ( GenerateRandom ( 10 ) % 7 == 0 )
            return new Request();
    }
    return NULL;
}

void ProcessRequest ( Request* /*request*/, Stopper stopSignal )
{
    while ( !stopSignal )
    {
        ::Sleep ( GenerateRandom ( 5 ) );
        if ( GenerateRandom ( 5 ) % 4 == 0 )
            return;
    }
}

void DeleteRequest ( Request* request )
{
    if ( request )
        delete request;
}
