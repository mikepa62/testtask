#include "helpers.h"

#include <Windows.h>

#include <iostream>
#include <random>
#include <sstream>

int GenerateRandom ( unsigned int upper_bound )
{
    std::random_device              rd;
    std::mt19937                    gen ( rd () );
    std::uniform_int_distribution<> dis ( 1, upper_bound );
    return dis ( gen );
}

unsigned long GetLastWinApiError ()
{
    DWORD             last_error_code = ::GetLastError ();
    std::stringstream error_strm;
    error_strm << L"0x" << std::uppercase << std::hex << last_error_code;

    const unsigned int MAX_BUF_SIZE = 1024;
    char               error_text_buf[MAX_BUF_SIZE];
    DWORD              format_flags = FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;
    DWORD              lang_id = MAKELANGID ( LANG_NEUTRAL, SUBLANG_DEFAULT );
    if ( ::FormatMessage ( format_flags, NULL, last_error_code, lang_id, error_text_buf, MAX_BUF_SIZE, NULL ) )
        error_strm << L": " << error_text_buf;
    std::cerr << error_strm.str ();
    return last_error_code;
}